import request from "@/utils/request.js";

// 获取收货地址
export function memberAddress() {
	return request({
		url: `/jeecg-boot/member/memberAddress/list/mp`,
		method: 'get',
	})
}

// 增加收货地址
export function addAddress(data) {
	return request({
		url: `/jeecg-boot/member/memberAddress/add`,
		method: 'post',
		data
	})
}

// 编辑收货地址
export function editAddress(data) {
	return request({
		url: `/jeecg-boot/member/memberAddress/edit`,
		method: 'put',
		data
	})
}

// 删除收货地址
export function deleteAddress(id) {
	return request({
		url: `/jeecg-boot/member/memberAddress/delete?id=${id}`,
		method: 'delete'
	})
}

// id查询地址详情
export function queryById(data) {
	return request({
		url: `/jeecg-boot/member/memberAddress/queryById`,
		method: 'get',
		data
	})
}


// id查询用户详情
export function queryAccountById(data) {
	return request({
		url: `/jeecg-boot/member/memberAccount/queryById`,
		method: 'get',
		data
	})
}

// 获取认养订单列表
export function adoptOrderList(data) {
	return request({
		url: `/jeecg-boot/adopt/adoptOrder/list/mp`,
		method: 'get',
		data
	})
}


// 获取认养详情列表
export function getOrderDetail(data) {
	return request({
		url: `/jeecg-boot/adopt/adoptOrder/getOrderDetail`,
		method: 'get',
		data
	})
}

// 测试
export function getOrderDetail1(data) {
	return request({
		url: `/jeecg-boot/sys/dict/list`,
		method: 'get',
		data
	})
}

// 测试
export function logout() {
	return request({
		url: `/jeecg-boot/weChat/logout`,
		method: 'post',
	})
}

// 优惠券列表
export function marketCoupon(data) {
	return request({
		url: `/jeecg-boot/market/marketCoupon/list/mp`,
		method: 'get',
		data
	})
}

// 字典表
export function getDictItems(dictCode) {
	return request({
		url: `/jeecg-boot/sys/dict/getByCode/${dictCode}`,
		method: 'get',
	})
}

// 领取优惠券
export function receive(data) {
	return request({
		url: `/jeecg-boot/market/marketMemberCoupon/receive`,
		method: 'post',
		data
	})
}

// 查询自己的优惠券
export function myReceive(data) {
	return request({
		url: `/jeecg-boot/market/marketMemberCoupon/list/mp`,
		method: 'get',
		data
	})
}

// 查询自己可用的优惠券
export function getAllowUseList(data) {
	return request({
		url: `/jeecg-boot/market/marketMemberCoupon/getAllowUseList`,
		method: 'get',
		data
	})
}

// 查询自己商城可用的优惠券
export function getGoodsAllowUseList(data) {
	return request({
		url: `/jeecg-boot/market/marketMemberCoupon/getGoodsAllowUseList`,
		method: 'post',
		data
	})
}

// 查询问题分类列表
export function basisQuestionCategory() {
	return request({
		url: `/jeecg-boot/basis/basisQuestionCategory/list/mp`,
		method: 'get',
	})
}

// 查询问题列表
export function basisQuestion(data) {
	return request({
		url: `/jeecg-boot/basis/basisQuestion/list/mp`,
		method: 'get',
		data
	})
}

// 获取当前地区
export function getRegion() {
	return request({
		url: `/jeecg-boot/basis/basisRegion/getRegion`,
		method: 'get',
	})
}


// 获取订单状态数量
export function getOrderCount() {
	return request({
		url: `/jeecg-boot/goods/goodsOrder/getOrderCount`,
		method: 'get',
	})
}

// 余额充值
export function balanceRecharge(data) {
	return request({
		url: `/jeecg-boot/member/memberAccount/balanceRecharge`,
		method: 'post',
		data
	})
}

// 充值记录
export function memberRechargeRecord(data) {
	return request({
		url: `/jeecg-boot/member/memberRechargeRecord/list/mp`,
		method: 'get',
		data
	})
}

// 消费记录
export function memberConsumeRecord(data) {
	return request({
		url: `/jeecg-boot/member/memberConsumeRecord/list/mp`,
		method: 'get',
		data
	})
}

// 查看余额充值结果
export function queryRechargeResult(data) {
	return request({
		url: `/jeecg-boot/member/memberAccount/queryRechargeResult`,
		method: 'post',
		data
	})
}

// 认养工单列表
export function adoptWorkTask(data) {
	return request({
		url: `/jeecg-boot/adopt/adoptWorkTask/list/mp`,
		method: 'get',
		data
	})
}

// 认养工单详情
export function listAdoptWorkTaskDetailByMainId(data) {
	return request({
		url: `/jeecg-boot/adopt/adoptWorkTask/listAdoptWorkTaskDetailByMainId`,
		method: 'get',
		data
	})
}

// 认养工单添加
export function adoptWorkComplete(data) {
	return request({
		url: `/jeecg-boot/adopt/adoptWorkTask/complete`,
		method: 'post',
		data
	})
}

// 流水记录
export function memberWaterRecord(data) {
	return request({
		url: `/jeecg-boot/member/memberWaterRecord/list/mp`,
		method: 'get',
		data
	})
}

// 认养退款
export function getRefundPrice(data) {
	return request({
		url: `/jeecg-boot/adopt/adoptAfterOrder/getRefundPrice`,
		method: 'get',
		data
	})
}

// 退款申请
export function refundApply(data) {
	return request({
		url: `/jeecg-boot/adopt/adoptAfterOrder/apply`,
		method: 'post',
		data
	})
}

// 尾款支付
export function balanceOrder(data) {
	return request({
		url: `/jeecg-boot/adopt/adoptOrder/balanceOrder`,
		method: 'post',
		data
	})
}

// 认养退款售后
export function adoptAfterOrder(data) {
	return request({
		url: `/jeecg-boot/adopt/adoptAfterOrder/list/mp`,
		method: 'get',
		data
	})
}


// 认养退款售后通过id查询
export function adoptAfterOrderQueryById(data) {
	return request({
		url: `/jeecg-boot/adopt/adoptAfterOrder/queryById`,
		method: 'get',
		data
	})
}


// 认养物流
export function getLogistics(data) {
	return request({
		url: `/jeecg-boot/adopt/adoptDeliveryOrder/getLogistics`,
		method: 'get',
		data
	})
}

// 积分分类
export function goodsScoreCategory(data) {
	return request({
		url: `/jeecg-boot/market/goodsScoreCategory/list/mp`,
		method: 'get',
		data
	})
}

// 积分商品
export function goodsScoreInfo(data) {
	return request({
		url: `/jeecg-boot/market/goodsScoreInfo/list/mp`,
		method: 'get',
		data
	})
}

// 积分兑换
export function exchange(data) {
	return request({
		url: `/jeecg-boot/market/goodsScoreOrder/exchange`,
		method: 'post',
		data
	})
}

// 积分记录
export function memberScoreRecord(data) {
	return request({
		url: `/jeecg-boot/member/memberScoreRecord/list/mp`,
		method: 'get',
		data
	})
}

// 积分商品记录
export function goodsScoreOrder(data) {
	return request({
		url: `/jeecg-boot/market/goodsScoreOrder/list/mp`,
		method: 'get',
		data
	})
}

// 积分商品id查询
export function goodsScoreOrderById(data) {
	return request({
		url: `/jeecg-boot/market/goodsScoreOrder/queryById`,
		method: 'get',
		data
	})
}

// 积分商品物流查询
export function goodsScoreOrderGetLogistics(data) {
	return request({
		url: `/jeecg-boot/market/goodsScoreOrder/getLogistics`,
		method: 'get',
		data
	})
}

// 积分商品确认收货
export function goodsScoreOrderComplete(data) {
	return request({
		url: `/jeecg-boot/market/goodsScoreOrder/complete`,
		method: 'post',
		data
	})
}





