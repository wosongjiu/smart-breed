import request from "@/utils/request.js";


// 获取商品分类
export function goodsCategory() {
	return request({
		url: `/jeecg-boot/api/page/goodsCategory`,
		method: 'get',
	})
}

// 获取商品信息
export function goodsList(data) {
	return request({
		url: `/jeecg-boot/api/page/goodsList`,
		method: 'get',
		data
	})
}

// 搜索商品信息
export function searchGoods(data) {
	return request({
		url: `/jeecg-boot/api/page/searchGoods`,
		method: 'get',
		data
	})
}

// 商品详情
export function shopDetail(data) {
	return request({
		url: `/jeecg-boot/api/page/getGoodsById`,
		method: 'get',
		data
	})
}

// 商品评论
export function goodsCommentList(data) {
	return request({
		url: `/jeecg-boot/api/page/goodsCommentList`,
		method: 'get',
		data
	})
}


// 推荐商品
export function recommendList(data) {
	return request({
		url: `/jeecg-boot/api/page/recommendList`,
		method: 'get',
		data
	})
}


// 获取当前商品优惠券
export function getCouponList(data) {
	return request({
		url: `/jeecg-boot/api/page/getCouponList`,
		method: 'get',
		data
	})
}

// 获取购物车商品
export function goodsCart(data) {
	return request({
		url: `/jeecg-boot/goods/goodsCart/list/mp`,
		method: 'get',
		data
	})
}

// 添加购物车
export function addCart(data) {
	return request({
		url: `/jeecg-boot/goods/goodsCart/add`,
		method: 'post',
		data
	})
}

// 购物车更新数量
export function operationCart(data) {
	return request({
		url: `/jeecg-boot/goods/goodsCart/operation`,
		method: 'post',
		data
	})
}

// 购物车删除单个商品
export function deleteCart(id) {
	return request({
		url: `/jeecg-boot/goods/goodsCart/delete?id=${id}`,
		method: 'delete'
	})
}

// 清空购物车
export function clearCart(data) {
	return request({
		url: `/jeecg-boot/goods/goodsCart/clearCart`,
		method: 'post',
		data
	})
}

// 清空购物车
export function calculate(data) {
	return request({
		url: `/jeecg-boot/goods/goodsCart/calculate`,
		method: 'post',
		data
	})
}

// 计算商品价格
export function computeOrderPrice(data) {
	return request({
		url: `/jeecg-boot/goods/goodsOrder/computeOrderPrice`,
		method: 'post',
		data
	})
}

// 商城下单
export function createOrder(data) {
	return request({
		url: `/jeecg-boot/goods/goodsOrder/createOrder`,
		method: 'post',
		data
	})
}

// 商城订单列表
export function goodsOrder(data) {
	return request({
		url: `/jeecg-boot/goods/goodsOrder/list/mp`,
		method: 'get',
		data
	})
}

// 取消商城订单
export function cancelOrder(data) {
	return request({
		url: `/jeecg-boot/goods/goodsOrder/cancelOrder`,
		method: 'post',
		data
	})
}

// 继续支付商城订单
export function continueToPay(data) {
	return request({
		url: `/jeecg-boot/goods/goodsOrder/continueToPay`,
		method: 'post',
		data
	})
}

// 继续支付商城订单
export function deleteOrder(id) {
	return request({
		url: `/jeecg-boot/goods/goodsOrder/delete?id=${id}`,
		method: 'delete',
	})
}


// 商城订单详情
export function queryById(data) {
	return request({
		url: `/jeecg-boot/goods/goodsOrder/queryById`,
		method: 'get',
		data
	})
}

// 商城订单确认收货
export function completeOrder(data) {
	return request({
		url: `/jeecg-boot/goods/goodsOrder/complete`,
		method: 'post',
		data
	})
}

// 商城订单评论
export function goodsComment(orderId, data) {
	return request({
		url: `/jeecg-boot/goods/goodsComment/submit/${orderId}`,
		method: 'post',
		data
	})
}

// 商城订单物流详情
export function getLogistics(data) {
	return request({
		url: `/jeecg-boot/goods/goodsOrder/getLogistics`,
		method: 'get',
		data
	})
}

// 商城订单申请退款
export function goodsAfterOrder(data) {
	return request({
		url: `/jeecg-boot/goods/goodsAfterOrder/apply`,
		method: 'post',
		data
	})
}

// 商城订单售后订单
export function goodsAfterOrderList(data) {
	return request({
		url: `/jeecg-boot/goods/goodsAfterOrder/list/mp`,
		method: 'get',
		data
	})
}

// 商城订单再次购买
export function addBatch(data) {
	return request({
		url: `/jeecg-boot/goods/goodsCart/addBatch`,
		method: 'post',
		data
	})
}
